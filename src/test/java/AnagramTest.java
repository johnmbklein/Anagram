import org.junit.*;
import static org.junit.Assert.*;

public class AnagramTest {

  @Test
  public void checkAnagram_notAnagram_False(){
    Anagram testAnagram = new Anagram();
    Boolean expected = false;
    assertEquals(expected, testAnagram.checkAnagram("dog", "cat"));
  }

  @Test
  public void checkAnagram_anAnagram_True(){
    Anagram testAnagram = new Anagram();
    Boolean expected = true;
    assertEquals(expected, testAnagram.checkAnagram("cat", "act"));
  }

  @Test
  public void checkAnagram_multipleLettersNotAnagram_False(){
    Anagram testAnagram = new Anagram();
    Boolean expected = false;
    assertEquals(expected, testAnagram.checkAnagram("ttt", "cat"));
  }

}
